AWIT-CustomerNotes
==================
This plugin extends OTRS to provide a customer-specific means to add notes which are displayed for each ticket
associated with the customer's ID.

**Feature List**

* Notes are displayed at the top of each ticket if notes exist for the customer
* Two types of notes are supported, namely "important" and "normal"
* Notes which are disabled display with a strike-through
* Disabled notes display up to 3 months after being disabled, calculated from the last time of change

**Prerequisites**

- OTRS 5.0

**Installation**

Download the package and install it via the OTRS admin interface package manager.

**Configuration**

* There is currently no configuration for this module.

**Download**

For download see [http://allworldit.software/projects-otrs](http://allworldit.software/projects-otrs).

**Commercial Support & Development**

Support for this extension and OTRS is available from [http://allworldit.com](http://allworldit.com).

