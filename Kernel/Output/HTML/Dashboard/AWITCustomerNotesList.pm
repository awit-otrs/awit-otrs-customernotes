# --
# Customer Notes List
# Copyright (C) 2015, AllWorldIT
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::Output::HTML::Dashboard::AWITCustomerNotesList;

use strict;
use warnings;

our $ObjectManagerDisabled = 1;



sub new {
	my ( $Type, %Param ) = @_;

	my $Self = {%Param};
	bless( $Self, $Type );

	for my $Needed (qw(Config Name UserID)) {
		die "Got no $Needed!" if ( !$Self->{$Needed} );
	}

	# Get web request
	my $ParamObject = $Kernel::OM->Get('Kernel::System::Web::Request');

	# Get current filter
	my $Name = $ParamObject->GetParam( Param => 'Name' ) || '';
	my $PreferencesKey = 'UserDashboardCustomerNotesListFilter' . $Self->{Name};

	$Self->{PrefKey} = 'UserDashboardPref' . $Self->{Name} . '-Shown';

	$Self->{PageShown} = $Kernel::OM->Get('Kernel::Output::HTML::Layout')->{ $Self->{PrefKey} }
			|| $Self->{Config}->{Limit};

	$Self->{StartHit} = int( $ParamObject->GetParam( Param => 'StartHit' ) || 1 );

	return $Self;
}



sub Preferences
{
	my ( $Self, %Param ) = @_;


	my @Params = (
		{
			Desc => 'Shown notes',
			Name => $Self->{PrefKey},
			Block => 'Option',

			Data => {
				5  => ' 5',
				10 => '10',
				15 => '15',
				20 => '20',
				25 => '25',
			},

			SelectedID => $Self->{PageShown},
			Translation => 0,
		},
	);

	return @Params;
}


sub Config
{
	my ( $Self, %Param ) = @_;

	return (
		%{ $Self->{Config} },
		# Disable page cache due to filter
		CacheTTL => undef,
		CacheKey => undef,
	);
}



sub Run {
	my ( $Self, %Param ) = @_;


	return if !$Param{CustomerID};

	my $CustomerUserObject = $Kernel::OM->Get('Kernel::System::CustomerUser');
	my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
	my $NotesObject = $Kernel::OM->Get('Kernel::System::AWITCustomerNotes');


	my @Notes = $NotesObject->NotesList(CustomerID => $Param{CustomerID});

	my $AddAccess = $LayoutObject->Permission(
		Action => 'AdminAWITCustomerNotes',
		Type => 'rw', # ro|rw possible
	);

	my $Content = $LayoutObject->Output(
		TemplateFile => 'AgentDashboardAWITCustomerNotesList',
		Data => {
			HasRW => $AddAccess,
			CustomerID => $Param{CustomerID},
			NotesList => \@Notes,
		},
	);

	return $Content;
}



1;
