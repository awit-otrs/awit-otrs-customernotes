# --
# AllWorldIT Customer Notes Module
# Copyright (C) 2015, AllWorldIT.
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::Output::HTML::AWITCustomerNotes;

use strict;
use warnings;

our @ObjectDependencies = qw(
	Kernel::Config
	Kernel::System::DB
	Kernel::Output::HTML::Layout
	Kernel::System::Web::Request
);



sub new {
	my ( $Type, %Param ) = @_;


	my $Self = {%Param};
	bless( $Self, $Type );

	# Grab TicketID
	$Self->{TicketID} = $Param{TicketID};

	return $Self;
}



sub Run {
	my ( $Self, %Param ) = @_;


	return if !$Self->{TicketID};

	my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
	my $DBObject	 = $Kernel::OM->Get('Kernel::System::DB');
	my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
	my $ParamObject  = $Kernel::OM->Get('Kernel::System::Web::Request');
	my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
	my $NotesObject = $Kernel::OM->Get('Kernel::System::AWITCustomerNotes');

	# Grab ticket
        my %Ticket = $TicketObject->TicketGet(
		TicketID => $Self->{'TicketID'},
        );
	if (!%Ticket) {
		$Kernel::OM->Get('Kernel::System::Log')->Log(
			Priority => 'error',
			Message  => 'Ticket not retrieved!',
		);
		return;
	}


	# Grab list of notes
	my @Notes = $NotesObject->NotesList(CustomerID => $Ticket{CustomerID});


	my $HTML = $LayoutObject->Output(
		TemplateFile => 'AgentTicketZoomAWITCustomerNotes',
		Data	 => {
			NotesList => \@Notes,
		},
	);


	# Add notes
	${ $Param{Data} } =~ s{
		(
			</div>\s+
		)
		(
			<div\ id="ArticleTree">
		)
	 }{ $1 $HTML $2 }ixms;

	return $Param{Data};
}



1;
