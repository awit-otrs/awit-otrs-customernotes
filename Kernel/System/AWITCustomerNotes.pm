#--
# AWIT-CustomerNotes Plugin for OTRS
# Copyright (C) 2015, AllWorldIT
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::AWITCustomerNotes;

use strict;
use warnings;

our @ObjectDependencies = (
	'Kernel::Config',
	'Kernel::System::DB',
	'Kernel::System::Log',
	'Kernel::System::Time',
	'Kernel::System::User',
);



=head1 NAME

Kernel::System::AWITCustomerNotes - Customer notes functionality

=head1 SYNOPSIS

All customer notes functionality.

=head1 PUBLIC INTERFACE

=over 4



=item new()

Create an object

  use Kernel::System::ObjectManager;
  local $Kernel::OM = Kernel::System::ObjectManager->new();
  my $NotesObject = $Kernel::OM->Get('Kernel::System::AWITCustomerNotes');

=cut

sub new
{
	my ($Type,%Param ) = @_;

	my $Self = {};
	bless($Self,$Type);

	return $Self;
}



=item NotesList()

Get a list of notes associated with this customer

  my %Notes = $NotesObject->NotesList(
  	CustomerID => 'custhere'
  );

Returns:

  %Notes = [
  	{
  		ID => 1,
  		CreateTime => '2015-12-15 11:57:01',
  		CreateBy => 2,
  		ChangeTime => '2015-12-15 11:57:01',
  		ChangeBy => 2,
  		Note => 'hello world',
  		Type => 1,
  		Disabled => 0,
  	}
  ];

=cut

sub NotesList
{
	my ($Self,%Param) = @_;

	# Check for params we need
	if ( !$Param{CustomerID} ) {
		$Kernel::OM->Get('Kernel::System::Log')->Log(
			Priority => 'error',
			Message  => 'Need CustomerID!',
		);
		return;
	}

	# Get user object
	my $UserObject = $Kernel::OM->Get('Kernel::System::User');

	# Get DB object
	my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

	# Get notes from database
	return if !$DBObject->Prepare(
		SQL => '
			SELECT
				id,
				customer_id,
				create_time, create_by,
				change_time, change_by,
				note, type,
				disabled
			FROM
				awit_customernotes
			WHERE
				customer_id = ?
				AND (
					disabled = 0 OR (
						disabled = 1 AND
						change_time > NOW() - INTERVAL 3 MONTH
					)
				)
			ORDER BY
				disabled ASC, type ASC, change_time DESC
		',
		Bind => [ \$Param{CustomerID} ],
	);

	my @Data;
	# Pull rows
	while ( my @Row = $DBObject->FetchrowArray() ) {
		# Resolve user names
        	my $CreateByUserName = $UserObject->UserName(
	            UserID => $Row[3],
	        );
        	my $ChangeByUserName = $UserObject->UserName(
	            UserID => $Row[5],
	        );
		# Create note
	        my %Note = (
			ID => $Row[0],
			CustomerID => $Row[1],
			CreateTime => $Row[2],
			CreateBy => $Row[3],
			CreateByUserName => $CreateByUserName,
			ChangeTime => $Row[4],
			ChangeBy => $Row[5],
			ChangeByUserName => $ChangeByUserName,
			Note => $Row[6],
			Type => $Row[7],
			Disabled => $Row[8],
		);
		# Add note to list
		push @Data, \%Note;
	}


	return @Data;
}



=item NotesGet()

Get a specific note

  my %Notes = $NotesObject->NotesGet(
  	ID => 77
  );

Returns:

  %Notes = {
  	ID => 77,
  	CreateTime => '2015-12-15 11:57:01',
  	CreateBy => 2,
  	ChangeTime => '2015-12-15 11:57:01',
  	ChangeBy => 2,
  	Note => 'hello world',
  	Type => 1,
  	Disabled => 0,
  };

=cut

sub NotesGet
{
	my ($Self,%Param) = @_;

	# Check for params we need
	if ( !$Param{ID} ) {
		$Kernel::OM->Get('Kernel::System::Log')->Log(
			Priority => 'error',
			Message  => 'Need ID!',
		);
		return undef;
	}

	# Get user object
	my $UserObject = $Kernel::OM->Get('Kernel::System::User');

	# Get DB object
	my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

	# Get notes from database
	return undef if !$DBObject->Prepare(
		SQL => '
			SELECT
				id,
				customer_id,
				create_time, create_by,
				change_time, change_by,
				note, type,
				disabled
			FROM
				awit_customernotes
			WHERE
				id = ?
		',
		Bind => [ \$Param{ID} ],
	);

	my @Data;
	# Pull rows
	my @Row = $DBObject->FetchrowArray();

	return undef if (!@Row);

	# Resolve user names
       	my $CreateByUserName = $UserObject->UserName(
            UserID => $Row[3],
        );
       	my $ChangeByUserName = $UserObject->UserName(
            UserID => $Row[5],
        );
	# Create note
        my %Note = (
		ID => $Row[0],
		CustomerID => $Row[1],
		CreateTime => $Row[2],
		CreateBy => $Row[3],
		CreateByUserName => $CreateByUserName,
		ChangeTime => $Row[4],
		ChangeBy => $Row[5],
		ChangeByUserName => $ChangeByUserName,
		Note => $Row[6],
		Type => $Row[7],
		Disabled => $Row[8],
	);


	return %Note;
}



=item NotesAdd()

Insert note into DB

  $NotesObject->NotesAdd(
  	ID => 1,
  	CreateBy => 2,
  	Note => 'hello world',
  	Type => 1,
  	Disabled => 0,
  );

=cut

sub NotesAdd {
	my ($Self,%Param) = @_;


	for my $Needed (qw(CustomerID CreateBy Note Type)) {
		if ( !$Param{$Needed} ) {
			$Kernel::OM->Get('Kernel::System::Log')->Log(
				Priority => 'error',
				Message  => "NotesAdd: Need $Needed!"
			);
			return undef;
		}
	}

	# Set default
	my $Disabled = 0;
	if ( defined($Param{Disabled}) && $Param{Disabled} == 1 ) {
		$Disabled = $Param{Disabled};
	}

	# Build SQL query
        my $SQL = '
		INSERT INTO awit_customernotes
			(
				customer_id,
				create_time, create_by,
				change_time, change_by,
				note, type,
				disabled
			)
		VALUES 
			(
				?,
				current_timestamp, ?,
				current_timestamp, ?,
				?, ?,
				?
			)
	';
        my $Bind = [
		\$Param{CustomerID}, \$Param{CreateBy}, \$Param{CreateBy},
		\$Param{Note}, \$Param{Type},
		\$Disabled,
        ];

        # Do the DB insert
        return if !$Kernel::OM->Get('Kernel::System::DB')->Do(
            SQL  => $SQL,
            Bind => $Bind
        );

	return 1;
}



=item NotesUpdate()

Update note in DB

  $NotesObject->NotesUpdate(
  	ID => 1,
  	Note => 'hello world',
  	Type => 1,
  	Disabled => 1,
  );

=cut

sub NotesUpdate {
	my ($Self,%Param) = @_;


	for my $Needed (qw(ID Note Type ChangeBy)) {
		if ( !$Param{$Needed} ) {
			$Kernel::OM->Get('Kernel::System::Log')->Log(
				Priority => 'error',
				Message  => "NotesUpdate: Need $Needed!"
			);
			return undef;
		}
	}

	# Set default
	my $Disabled = 0;
	if ( defined($Param{Disabled}) && $Param{Disabled} == 1 ) {
		$Disabled = $Param{Disabled};
	}

	# Build SQL query
        my $SQL = '
		UPDATE awit_customernotes
		SET
			change_by = ?, change_time = current_timestamp,
			note = ?, type = ?,
			disabled = ?
		WHERE 
			id = ?
	';
        my $Bind = [
		\$Param{ChangeBy},
		\$Param{Note}, \$Param{Type},
		\$Disabled,
		\$Param{ID}
        ];

        # Do the DB insert
        return if !$Kernel::OM->Get('Kernel::System::DB')->Do(
            SQL  => $SQL,
            Bind => $Bind
        );

	return 1;
}



1;

=back

=head1 TERMS AND CONDITIONS

This software is part of the AWIT-CustomerNotes plugin.

This software comes with ABSOLUTELY NO WARRANTY. For details, see
the enclosed file COPYING for license information (AGPL). If you
did not receive this file, see L<http://www.gnu.org/licenses/agpl.txt>.

=cut
