# --
# AllWorldIT Customer Notes
# Copyright (C) 2015, AllWorldIT.
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::Modules::AdminAWITCustomerNotes;

use strict;
use warnings;


our $ObjectManagerDisabled = 1;



sub new
{
	my ($Type,%Param) = @_;


	my $Self = {%Param};
	bless($Self,$Type );


	# Record user ID
	$Self->{UserID} = $Param{UserID};


	return $Self;
}



sub Run {
	my ($Self,%Param) = @_;


	return if !$Self->{UserID};

	my $ParamObject = $Kernel::OM->Get('Kernel::System::Web::Request');
	my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
	my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
	my $NotesObject = $Kernel::OM->Get('Kernel::System::AWITCustomerNotes');

        # Header
        my $Output = $LayoutObject->Header();
        $Output .= $LayoutObject->NavigationBar();

	# Get parameters
	my %GetParam;
	for my $ParamName (qw(
		CustomerID CustomerNoteID CustomerNoteText CustomerNoteType CustomerNoteDisabled
	)) {
		$GetParam{$ParamName} = $ParamObject->GetParam( Param => $ParamName ) || '';
	}


	# Display change page
	if ( $Self->{Subaction} eq 'Change' ) {

		return if !$GetParam{CustomerNoteID};

		# Pull in note
		my %Note = $NotesObject->NotesGet(
			ID => $GetParam{CustomerNoteID},
		);

		# Display edit page
		$Output .= $Self->_Edit(
			Action => 'Change',
			%GetParam,
			CustomerNoteID => $Note{ID},
			CustomerNoteText => $Note{Note},
			CustomerNoteType => $Note{Type},
			CustomerNoteDisabled => $Note{Disabled},
		);


	# Change action
	} elsif ($Self->{Subaction} eq 'ChangeAction') {

		# Challenge token check for write action
		$LayoutObject->ChallengeTokenCheck();

		my %Errors = ();

		#
		# Validate form data
		#
		if ($GetParam{CustomerNoteID} eq "") {
			$Errors{CustomerNoteID} = "This field cannot be blank";
		}

		if ($GetParam{CustomerNoteText} eq "") {
			$Errors{CustomerNoteText} = "This field cannot be blank";
		}

		# Normal = 10, Important = 5
		if ($GetParam{CustomerNoteType} ne "10" && $GetParam{CustomerNoteType} ne "5") {
			$Errors{CustomerNoteType} = "This field has an invalid value, it must be either 'normal' (10) or 'important' (5)";
		}

		# Set disabled flag
		if ($GetParam{CustomerNoteDisabled} eq "" || $GetParam{CustomerNoteDisabled} eq "0") {
			$GetParam{CustomerNoteDisabled} = 0;
		} elsif ($GetParam{CustomerNoteDisabled} eq "1") {
			$GetParam{CustomerNoteDisabled} = 1;
		} else {
			$Errors{CustomerNoteDisabled} = "This field has an invalid value, it must be either '1' (true) or '0' (false)";
		}

		if (!%Errors) {
			# Try update note
    			if ($NotesObject->NotesUpdate(
				ID => $GetParam{CustomerNoteID},
				ChangeBy => $Self->{UserID},
				Note => $GetParam{CustomerNoteText},
				Type => $GetParam{CustomerNoteType},
				Disabled => $GetParam{CustomerNoteDisabled},
			# If it worked, redirect
			)) {
				return $LayoutObject->Redirect( OP => 'Action=AgentCustomerInformationCenter;CustomerID=' . $LayoutObject->LinkEncode($GetParam{CustomerID}) );
			# If not, display add page
			} else {
				$Output .= $Self->_Edit(
					Action => 'Change',
					Errors => \%Errors,
					%GetParam,
				);
			}
		}

		# Render the edit page again page if we have errors
		if (%Errors) {
			$Output .= $Self->_Edit(
				Action => 'Change',
				Errors => \%Errors,
				%GetParam,
			);
		}


	# Display add page
	} elsif ( $Self->{Subaction} eq 'Add' ) {

		$Output .= $Self->_Edit(
			Action => 'Add',
			%GetParam,
		);


	# add action
	} elsif ( $Self->{Subaction} eq 'AddAction' ) {

	        # Challenge token check for write action
	        $LayoutObject->ChallengeTokenCheck();

		my %Errors = ();


		#
		# Validate form data
		#
		if ($GetParam{CustomerNoteText} eq "") {
			$Errors{CustomerNoteText} = "This field cannot be blank";
		}

		# Normal = 10, Important = 5
		if ($GetParam{CustomerNoteType} ne "10" && $GetParam{CustomerNoteType} ne "5") {
			$Errors{CustomerNoteType} = "This field has an invalid value, it must be either 'normal' (10) or 'important' (5)";
		}

		# Set disabled flag
		if ($GetParam{CustomerNoteDisabled} eq "" || $GetParam{CustomerNoteDisabled} eq "0") {
			$GetParam{CustomerNoteDisabled} = 0;
		} elsif ($GetParam{CustomerNoteDisabled} eq "1") {
			$GetParam{CustomerNoteDisabled} = 1;
		} else {
			$Errors{CustomerNoteDisabled} = "This field has an invalid value, it must be either '1' (true) or '0' (false)";
		}

		if (!%Errors) {
			# Try add note
    			if ($NotesObject->NotesAdd(
				CustomerID => $GetParam{CustomerID},
				CreateBy => $Self->{UserID},
				Note => $GetParam{CustomerNoteText},
				Type => $GetParam{CustomerNoteType},
				Disabled => $GetParam{CustomerNoteDisabled},
			# If it worked, redirect
			)) {
				return $LayoutObject->Redirect( OP => 'Action=AgentCustomerInformationCenter;CustomerID=' . $LayoutObject->LinkEncode($GetParam{CustomerID}) );
			# If not, display add page
			} else {
				$Output .= $Self->_Edit(
					Action => 'Add',
					Errors => \%Errors,
					%GetParam,
				);
			}
		}

		# Render the edit page again page if we have errors
		if (%Errors) {
			$Output .= $Self->_Edit(
				Action => 'Add',
				Errors => \%Errors,
				%GetParam,
			);
		}


	# Unknown subaction, just redirect
	} else {
		# redirect to AdminCustomer
		return $LayoutObject->Redirect( OP => 'Action=AgentCustomerInformationCenter;CustomerID=' . $LayoutObject->LinkEncode($GetParam{CustomerID}) );
	};

	$Output .= $LayoutObject->Footer();

	return $Output;
}



sub _Edit {
    my ( $Self, %Param ) = @_;


    my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');

    return $LayoutObject->Output(
        TemplateFile => 'AdminAWITCustomerNotes',
        Data         => \%Param,
    );
}



1;
